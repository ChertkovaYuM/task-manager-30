package ru.tsc.chertkova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.command.domain.AbstractDomainCommand;
import ru.tsc.chertkova.tm.command.domain.DomainBackupLoadCommand;
import ru.tsc.chertkova.tm.command.domain.DomainBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es
            = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        if (!bootstrap.getAuthService().isAuth()) bootstrap.processCommand(DomainBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDomainCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DomainBackupLoadCommand.NAME, false);
    }

}
